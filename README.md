# HPCTREES

Welcome to running raster R scripts on in an HPC environment where you dont have root access to install libraries.

This repository contains 3 Scripts

Rsetupscript.sh
Rlibraryinstall.R
RapidHPCSubmissionScript.sh

You first need to install the R libraries you are going to need to run your script. However, you will also need to install the C/C++/Fortran libraries they are based on.

Firstly run "Rsetupscript.sh" using "bash Rsetupscript", dont worry about where you run it from as it will default your home directory upon execution.

This script will in order :

Download and install the C++ GDAL library
	 - This takes about an hour but only needs to be done once
Temporarily add PATHs using export functions
Download and install the Proj4 library
	 - This also takes an hour but only needs to be done once
Temporarily add PATHs using export functions
Load the Augusta R module (as of March 2019 there was only 1)
Call "Rlibraryinstall.R"

Next Rlibraryinstall.R runs to install the R libraries locally. This script will install landscapemetrics, raster and rgdal into your home directory, which needs doing only once.

Once this is done you need to add these lines to your .bashrc file, which you will find in your home director

export PATH=/gpfs01/home/$USER/bin:$PATH
export LD_LIBRARY_PATH=/gpfs01/home/$USER/lib:$LD_LIBRARY_PATH
export GDAL_DATA=/gpfs01/home/$USER/share/gdal
export INCLUDE=/gpfs01/home/$USER/include:$INCLUDE

this will allow R to find the libraries you just installed

You are now ready to run some jobs.

You do this by calling the script RapidHPCSubmissionScript.sh using "bash RapidHPCSubmissionScript.sh [Rasters] [Processes]" replacing the bracets with the appropraite values. For example if you want to analysi 10 rasters on 2 cores it would look like : "bash RapidHPCSubmissionScript.sh 10 2" 

Run this script from in the directory of your rasters so they can be seen!

RapidHPCSubmissionScript.sh will divide the number of rasters (N) by the number of processes (P) evenly and submit P jobs to the HPC with a walltime of N/P hours. This means the maximum number of rasters per process is 168 which will limit this script to 67200 rasters on 400 cores (the per user limit), if you have more rasters than this submit more jobs (they just wont run simultaneously).

Once the job starts it will create a log file called "Trees_M.log" where M will be replaced with the job number, which means 1 log file per job, which is alot of output, the R scripts will also create a separate output file per job so you will need to concatenate them at the end.

The script assumes you dont need more than 1gb of RAM per job which means unless you are using rasters of that size, you should be fine. 
