#!/bin/bash

module load R-uon/gcc6.3.0/3.5.1

Rasters=$1
Processes=$2

SCRIPTNAME="TreeRasters"
TIMEPERRASTER=60

for i in $(seq 1 1 $Processes)
do
    RASTERS=($(seq $i $Processes $Rasters ))
    
    # for (( j=0; j<${#RASTERS[@]}; j++ ));  do echo "${RASTERS[$j]}" ; done
    
    TIME=$(( $TIMEPERRASTER * ${#RASTERS[@]} ))
    
    HOURS=$(( $TIME / 60  ))
    
    if [[ $HOURS -gt "167" ]]; then
	echo "-- You have requested too many raster for a single core --"
	echo "-- maximum waltime will be reached prior to completion --"
	echo "-- submit with a higher number of requested processes --"
	exit
    fi

    MINUTES=$(( $TIME - $HOURS*60 ))

    TIME=$HOURS":"$MINUTES":00"

    ##########

    echo "#!/bin/bash" > $SCRIPTNAME"_slurm.sh"
    echo "#SBATCH --time="$TIME >> $SCRIPTNAME"_slurm.sh"
    echo "#SBATCH --job-name="$SCRIPTNAME >> $SCRIPTNAME"_slurm.sh"
    echo "#SBATCH --nodes=1" >> $SCRIPTNAME"_slurm.sh"
    echo "#SBATCH --ntasks-per-node=1" >> $SCRIPTNAME"_slurm.sh"
    echo "#SBATCH --mem=1g" >> $SCRIPTNAME"_slurm.sh"
    echo "" >> $SCRIPTNAME"_slurm.sh"
    echo "cd $""SLURM_SUBMIT_DIR" >> $SCRIPTNAME"_slurm.sh"
    echo "export OMP_NUM_THREADS=1" >> $SCRIPTNAME"_slurm.sh"
    echo "" >> $SCRIPTNAME"_slurm.sh"
    echo -n "Rscript FRAGSTATS_HPC.R " >> $SCRIPTNAME"_slurm.sh"; for (( j=0; j<${#RASTERS[@]}; j++ ));  do echo -n "${RASTERS[$j]} " >> $SCRIPTNAME"_slurm.sh"; done ; echo -n "> Trees_$i.log" >> $SCRIPTNAME"_slurm.sh"
    echo "" >> $SCRIPTNAME"_slurm.sh"
 
    ##########   

     sbatch $SCRIPTNAME"_slurm.sh"
 
done
