#!/bin/bash

GITDIRECTORY=$(pwd)

## Download and Install GDAL

cd # Put yourself 

if [ ! -f gdal-2.4.0.tar.gz ]; then
    wget http://download.osgeo.org/gdal/2.4.0/gdal-2.4.0.tar.gz
fi

tar -zxvf gdal-2.4.0.tar.gz

cd gdal-2.4.0

./configure --prefix=/gpfs01/home/$USER

make

make install

export PATH=/gpfs01/home/$USER/bin:$PATH
export LD_LIBRARY_PATH=/gpfs01/home/$USER/lib:$LD_LIBRARY_PATH
export GDAL_DATA=/gpfs01/home/$USER/share/gdal

## Download and Install PROJ

cd

if [ ! -f proj-5.2.0.tar.gz ];then
    wget http://download.osgeo.org/proj/proj-5.2.0.tar.gz
fi
if [ ! -f proj-datumgrid-1.8.zip ];then
    wget http://download.osgeo.org/proj/proj-datumgrid-1.8.zip
fi

tar -zxvf proj-5.2.0.tar.gz

cd proj-5.2.0

./configure --prefix=/gpfs01/home/$USER/

cd 

unzip proj-datumgrid-1.8.zip -d proj-5.2.0/nad/

cd proj-5.2.0

make 
make install

cd 
export INCLUDE=/gpfs01/home/$USER/include:$INCLUDE

## Download libraries

if [ ! -f raster_2.8-19.tar.gz ]; then
    wget https://cran.r-project.org/src/contrib/raster_2.8-19.tar.gz
fi
if [ ! -f landscapemetrics_0.3.1.tar.gz ]; then
    wget https://cran.r-project.org/src/contrib/landscapemetrics_0.3.1.tar.gz
fi 

## INSTALL R LIBRARIES

module load R-uon/gcc6.3.0/3.5.1

Rscript $GITDIRECTORY/Rlibraryinstall.R
