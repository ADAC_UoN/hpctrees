
.libPaths(getwd())
install.packages("landscapemetrics", repos="http://cran.r-project.org", lib=getwd())
install.packages("raster", repos="http://cran.r-project.org", lib=getwd())
install.packages("rgdal",repos="http://cran.r-project.org", lib=getwd(), configure.args=c('--with-proj-include=/gpfs01/home/$USER/include','--with-proj-lib=/gpfs01/home/$USER/lib'))